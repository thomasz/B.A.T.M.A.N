#####   AMIOT David
#####   AUFRRAY Malcolm
#####   NEGRONI Orso

##### IMPORT #####

import matplotlib.pyplot as plt

##### FONCTION #####

def Read_File(name):
    f = open(name, 'r')
    line = f.readline()
    line = line.split()
    Itime = line[2]
    data = []
    for line in f:
        data.append(line)
    f.close()
    return Itime, data

def Analysis_CPU(Itime, data):
    tab = []
    ts = []
    cpt = 0
    Itime = Itime/2.0
    for line in data:
        line = line.split()
        tmp = line[1].split(",")
        tmp = int(tmp[0]) + (int(tmp[1])/10.0)
        tab.append(tmp)
        ts.append(cpt)
        cpt = cpt + 1
    return tab, ts

def Analysis_Ping(Itime, data):
    tab = []
    ts = []
    cpt = 0
    for loop in range(0, len(data)):
        if(loop == 0):
            line = data[loop]
            line = line.split()
            addr = line[1]
        else:
            line = data[loop]
            line = line.split()
            if(line[1] == "bytes"):
                tmp = line[6]
                tmp = tmp.split("=")
                value = tmp[1]
                value = value.split(".")
                value = int(value[0]) + (int(value[1])/100.0)
            elif(line[0] == "Reply" or line[0] == "From"):
                value = 0
            tab.append(value)
            ts.append(cpt)
            cpt = cpt + 1
    return addr, tab, ts
    
def Analysis_Traceroute(Itime, data):
    tab1 = []
    tab2 = []
    tab3 = []
    ts = []
    cpt = -1
    for loop in range(0, len(data)):
        if(loop == 0):
            line = data[loop]
            line = line.split()
            addr = line[2]
        else:
            line = data[loop]
            line = line.split()
            if(line[0] != "traceroute"):
                if(line[0] != "*" and line[1] != "Destination"):
                    offset = 1
                    for loop in range(0, len(line)):                                
                        tmp = line[loop]
                        if(tmp == "*"):
                            if(offset == 1):
                                value1 = -50
                                offset = offset + 1
                            elif(offset == 2):
                                value2 = -50
                                offset = offset + 1
                            else:
                                value3 = -50
                                offset = offset + 1
                        elif(tmp == "ms"):
                            tmp = line[loop-1]
                            if(offset == 1):
                                value1 = tmp.split(".")
                                value1 = int(value1[0]) + (int(value1[1])/1000.0)
                                offset = offset + 1
                            elif(offset == 2):
                                value2 = tmp.split(".")
                                value2 = int(value2[0]) + (int(value2[1])/1000.0)
                                offset = offset + 1
                            else:
                                value3 = tmp.split(".")
                                value3 = int(value3[0]) + (int(value3[1])/1000.0)
                                offset = offset + 1
                        else:
                            offset = offset
                else:
                    value1 = -50
                    value2 = -50
                    value3 = -50
                tab1.append(value1)
                tab2.append(value2)
                tab3.append(value3)
                ts.append(cpt)
            else:
                cpt = cpt + 1
    return addr, tab1, tab2, tab3, ts

def Analysis_Statistic(Itime, data):
    tab_tx = []
    tab_tx_bytes = []
    tab_tx_dropped = []
    tab_rx = []
    tab_rx_bytes = []
    tab_forward = []
    tab_forward_bytes = []
    tab_mgmt_tx = []
    tab_mgmt_tx_bytes = []
    tab_mgmt_rx = []
    tab_mgmt_rx_bytes = []
    tab_frag_tx = []
    tab_frag_tx_bytes = []
    tab_frag_rx = []
    tab_frag_rx_bytes = []
    tab_frag_fwd = []
    tab_frag_fwd_bytes = []
    tab_tt_request_tx = []
    tab_tt_request_rx = []
    tab_tt_response_tx = []
    tab_tt_response_rx = []
    tab_tt_roam_adv_tx = []
    tab_tt_roam_adv_rx = []
    tab_dat_get_tx = []
    tab_dat_get_rx = []
    tab_dat_put_tx = []
    tab_dat_put_rx = []
    tab_dat_cached_reply_tx = []
    tab_nc_code = []
    tab_nc_code_bytes = []
    tab_nc_recode = []
    tab_nc_recode_bytes = []
    tab_nc_buffer = []
    tab_nc_decode = []
    tab_nc_decode_bytes = []
    tab_nc_decode_failed = []
    tab_nc_sniffed = []
    ts = []
    cpt = 0
    for loop in range(0, len(data)):
        line = data[loop]
        line = line.split()
        champs = line[0]
        value = line[1]
        if(champs == "tx:"):
            tab_tx.append(value)
        elif(champs == "tx_bytes:"):
            tab_tx_bytes.append(value)
        elif(champs == "tx_dropped:"):
            tab_tx_dropped.append(value)
        elif(champs == "rx:"):
            tab_rx.append(value)
        elif(champs == "rx_bytes:"):
            tab_rx_bytes.append(value)
        elif(champs == "forward:"):
            tab_forward.append(value)
        elif(champs == "forward_bytes:"):
            tab_forward_bytes.append(value)
        elif(champs == "mgmt_tx:"):
            tab_mgmt_tx.append(value)
        elif(champs == "mgmt_tx_bytes:"):
            tab_mgmt_tx_bytes.append(value)
        elif(champs == "mgmt_rx:"):
            tab_mgmt_rx.append(value)
        elif(champs == "mgmt_rx_bytes:"):
            tab_mgmt_rx_bytes.append(value)
        elif(champs == "frag_tx:"):
            tab_frag_tx.append(value)
        elif(champs == "frag_tx_bytes:"):
            tab_frag_tx_bytes.append(value)
        elif(champs == "frag_rx:"):
            tab_frag_rx.append(value)
        elif(champs == "frag_rx_bytes:"):
            tab_frag_rx_bytes.append(value)
        elif(champs == "frag_fwd:"):
            tab_frag_fwd.append(value)
        elif(champs == "frag_fwd_bytes:"):
            tab_frag_fwd_bytes.append(value)
        elif(champs == "tt_request_tx:"):
           tab_tt_request_tx .append(value)
        elif(champs == "tt_request_rx:"):
           tab_tt_request_rx.append(value)
        elif(champs == "tt_response_tx:"):
            tab_tt_response_tx.append(value)
        elif(champs == "tt_response_rx:"):
            tab_tt_response_rx.append(value)
        elif(champs == "tt_roam_adv_tx:"):
            tab_tt_roam_adv_tx.append(value)
        elif(champs == "tt_roam_adv_rx:"):
            tab_tt_roam_adv_rx.append(value)
        elif(champs == "dat_get_tx:"):
            tab_dat_get_tx.append(value)
        elif(champs == "dat_get_rx:"):
            tab_dat_get_rx.append(value)
        elif(champs == "dat_put_tx:"):
            tab_dat_put_tx.append(value)
        elif(champs == "dat_put_rx:"):
            tab_dat_put_rx.append(value)
        elif(champs == "dat_cached_reply_tx:"):
            tab_dat_cached_reply_tx.append(value)
        elif(champs == "nc_code:"):
            tab_nc_code.append(value)
        elif(champs == "nc_code_bytes:"):
            tab_nc_code_bytes.append(value)
        elif(champs == "nc_recode:"):
            tab_nc_recode.append(value)
        elif(champs == "nc_recode_bytes:"):
            tab_nc_recode_bytes.append(value)
        elif(champs == "nc_buffer:"):
            tab_nc_buffer.append(value)
        elif(champs == "nc_decode:"):
            tab_nc_decode.append(value)
        elif(champs == "nc_decode_bytes:"):
            tab_nc_decode_bytes.append(value)
        elif(champs == "nc_decode_failed:"):
            tab_nc_decode_failed.append(value)
        elif(champs == "nc_sniffed:"):
            tab_nc_sniffed.append(value)
        else:
            ts.append(cpt)
            cpt = cpt + 1
    return tab_tx, tab_tx_bytes, tab_tx_dropped, tab_rx, tab_rx_bytes, tab_forward, tab_forward_bytes, tab_mgmt_tx, tab_mgmt_tx_bytes, tab_mgmt_rx, tab_mgmt_rx_bytes, tab_frag_tx, tab_frag_tx_bytes, tab_frag_rx, tab_frag_rx_bytes, tab_frag_fwd, tab_frag_fwd_bytes, tab_tt_request_tx, tab_tt_request_rx, tab_tt_response_tx, tab_tt_response_rx, tab_tt_roam_adv_tx, tab_tt_roam_adv_rx, tab_dat_get_tx, tab_dat_get_rx, tab_dat_put_tx, tab_dat_put_rx, tab_dat_cached_reply_tx, tab_nc_code, tab_nc_code_bytes, tab_nc_recode, tab_nc_recode_bytes, tab_nc_buffer, tab_nc_decode, tab_nc_decode_bytes, tab_nc_decode_failed, tab_nc_sniffed, ts

##### PROGRAMME #####

## TEST CPU

Itime, data = Read_File("cputest.txt")
s = "For the CPU test the time interval is " + Itime + " second "
print(s)
tab, ts = Analysis_CPU(int(Itime), data)
plt.figure(1)
plt.xlabel('time (s)')
plt.ylabel('%CPU')
plt.title('Graph of %CPU')
plt.plot(ts, tab)
plt.savefig('cputest.png')
plt.close()

## TEST PING

Itime, data = Read_File("pingtest.txt")
s = "For the Ping test the time interval is " + Itime + " second "
print(s)
addr, tab, ts = Analysis_Ping(int(Itime), data)
s = "The recipient of the ping is " + addr
print(s)
plt.figure(1)
plt.xlabel('number ping')
plt.ylabel('time (ms)')
plt.title('Graph of Ping with ' + addr)
plt.plot(ts, tab)
plt.savefig('pingtest.png')
plt.close()

## TEST TRACEROUTE

Itime, data = Read_File("traceroutetest.txt")
s = "For the traceroute test the time interval is " + Itime + " second "
print(s)
addr, tab1, tab2, tab3, ts = Analysis_Traceroute(int(Itime), data)
s = "The recipient of the traceroute is " + addr
print(s)

plt.figure(1)
plt.xlabel('number traceroute')
plt.ylabel('time (ms)')
plt.title('Graph of traceroute (Big packet)')
plt.scatter(ts, tab1, s=10, c="red")
plt.savefig('traceroute1test.png')
plt.close()
 
plt.figure(1)
plt.xlabel('number traceroute')
plt.ylabel('time (ms)')
plt.title('Graph of traceroute (Medium packet)')
plt.scatter(ts, tab2, s=10, c="green")
plt.savefig('traceroute2test.png')
plt.close()

plt.figure(1)
plt.xlabel('number traceroute')
plt.ylabel('time (ms)')
plt.title('Graph of traceroute (small packet)')
plt.scatter(ts, tab3, s=10, c="blue")
plt.savefig('traceroute3test.png')
plt.close()

## TEST STATISTICS

Itime, data = Read_File("statisticstest.txt")
tab_tx, tab_tx_bytes, tab_tx_dropped, tab_rx, tab_rx_bytes, tab_forward, tab_forward_bytes, tab_mgmt_tx, tab_mgmt_tx_bytes, tab_mgmt_rx, tab_mgmt_rx_bytes, tab_frag_tx, tab_frag_tx_bytes, tab_frag_rx, tab_frag_rx_bytes, tab_frag_fwd, tab_frag_fwd_bytes, tab_tt_request_tx, tab_tt_request_rx, tab_tt_response_tx, tab_tt_response_rx, tab_tt_roam_adv_tx, tab_tt_roam_adv_rx, tab_dat_get_tx, tab_dat_get_rx, tab_dat_put_tx, tab_dat_put_rx, tab_dat_cached_reply_tx, tab_nc_code, tab_nc_code_bytes, tab_nc_recode, tab_nc_recode_bytes, tab_nc_buffer, tab_nc_decode, tab_nc_decode_bytes, tab_nc_decode_failed, tab_nc_sniffed, ts = Analysis_Statistic(int(Itime), data)

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('packet')
plt.title('Graph of tx')
plt.plot(ts, tab_tx)
plt.savefig('tab_txtest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('byte')
plt.title('Graph of tx_bytes')
plt.plot(ts, tab_tx_bytes)
plt.savefig('tab_tx_bytestest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('packet')
plt.title('Graph of tx_dropped')
plt.plot(ts, tab_tx_dropped)
plt.savefig('tab_tx_droppedtest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('packet')
plt.title('Graph of rx')
plt.plot(ts, tab_rx)
plt.savefig('tab_rxtest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('byte')
plt.title('Graph of rx_bytes ')
plt.plot(ts, tab_rx_bytes)
plt.savefig('tab_rx_bytestest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('packet')
plt.title('Graph of forward')
plt.plot(ts, )
plt.savefig('tab_forwardtest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('byte')
plt.title('Graph of tab_forward_bytes')
plt.plot(ts, tab_forward_bytes)
plt.savefig('tab_forward_bytestest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('packet')
plt.title('Graph of mgmt_tx')
plt.plot(ts, tab_mgmt_tx)
plt.savefig('tab_mgmt_txtest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('byte')
plt.title('Graph of mgmt_tx_bytes')
plt.plot(ts, tab_mgmt_tx_bytes)
plt.savefig('tab_mgmt_tx_bytestest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('packet')
plt.title('Graph of mgmt_rx')
plt.plot(ts, tab_mgmt_rx)
plt.savefig('tab_mgmt_rxtest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('byte')
plt.title('Graph of tab_mgmt_rx_bytes')
plt.plot(ts, tab_mgmt_rx_bytes)
plt.savefig('tab_mgmt_rx_bytestest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('packet')
plt.title('Graph of frag_tx')
plt.plot(ts, tab_frag_tx)
plt.savefig('tab_frag_txtest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('byte')
plt.title('Graph of frag_tx_bytes')
plt.plot(ts, tab_frag_tx_bytes)
plt.savefig('tab_frag_tx_bytestest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('packet')
plt.title('Graph of frag_rx')
plt.plot(ts, tab_frag_rx)
plt.savefig('tab_frag_rxtest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('byte')
plt.title('Graph of frag_rx_bytes')
plt.plot(ts, tab_frag_rx_bytes)
plt.savefig('tab_frag_rx_bytestest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('packet')
plt.title('Graph of frag_fwd')
plt.plot(ts, tab_frag_fwd)
plt.savefig('tab_frag_fwdtest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('byte')
plt.title('Graph of frag_fwd_bytes')
plt.plot(ts, tab_frag_fwd_bytes)
plt.savefig('tab_frag_fwd_bytestest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('request')
plt.title('Graph of tt_request_tx')
plt.plot(ts, tab_tt_request_tx)
plt.savefig('tab_tt_request_txtest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('request')
plt.title('Graph of tt_request_rx')
plt.plot(ts, tab_tt_request_rx)
plt.savefig('tab_tt_request_rxtest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('reponse')
plt.title('Graph of tt_response_tx')
plt.plot(ts, tab_tt_response_tx)
plt.savefig('tab_tt_response_txtest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('reponse')
plt.title('Graph of tt_response_rx')
plt.plot(ts, tab_tt_response_rx)
plt.savefig('tab_tt_response_rxtest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('packet')
plt.title('Graph of tt_roam_adv_tx')
plt.plot(ts, tab_tt_roam_adv_tx)
plt.savefig('tab_tt_roam_adv_txtest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('packet')
plt.title('Graph of tt_roam_adv_rx')
plt.plot(ts, tab_tt_roam_adv_rx)
plt.savefig('tab_tt_roam_adv_rxtest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('data get')
plt.title('Graph of dat_get_tx')
plt.plot(ts, tab_dat_get_tx)
plt.savefig('tab_dat_get_txtest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('data get')
plt.title('Graph of dat_get_rx')
plt.plot(ts, tab_dat_get_rx)
plt.savefig('tab_dat_get_rxtest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('data put')
plt.title('Graph of dat_put_tx')
plt.plot(ts, tab_dat_put_tx)
plt.savefig('tab_dat_put_txtest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('data put')
plt.title('Graph of dat_put_rx')
plt.plot(ts, tab_dat_put_rx)
plt.savefig('tab_dat_put_rxtest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('reply cached')
plt.title('Graph of dat_cached_reply_tx')
plt.plot(ts, tab_dat_cached_reply_tx)
plt.savefig('tab_dat_cached_reply_txtest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('packet')
plt.title('Graph of nc_code')
plt.plot(ts, tab_nc_code)
plt.savefig('tab_nc_codetest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('byte')
plt.title('Graph of nc_code_bytes')
plt.plot(ts, tab_nc_code_bytes)
plt.savefig('tab_nc_code_bytestest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('packet')
plt.title('Graph of nc_recode')
plt.plot(ts, tab_nc_recode)
plt.savefig('tab_nc_recodetest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('byte')
plt.title('Graph of nc_recode_bytes')
plt.plot(ts, tab_nc_recode_bytes)
plt.savefig('tab_nc_recode_bytestest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('packet')
plt.title('Graph of nc_buffer')
plt.plot(ts, tab_nc_buffer)
plt.savefig('tab_nc_buffertest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('packet')
plt.title('Graph of nc_decode')
plt.plot(ts, tab_nc_decode)
plt.savefig('tab_nc_decodetest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('byte')
plt.title('Graph of nc_decode_bytes')
plt.plot(ts, tab_nc_decode_bytes)
plt.savefig('tab_nc_decode_bytestest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('packet')
plt.title('Graph of nc_decode_failed')
plt.plot(ts, tab_nc_decode_failed)
plt.savefig('tab_nc_decode_failedtest.png')
plt.close()

plt.figure(1)
plt.xlabel('number of intake ')
plt.ylabel('packet')
plt.title('Graph of nc_sniffed')
plt.plot(ts, tab_nc_sniffed)
plt.savefig('tab_nc_sniffedtest.png')
plt.close()
