#----------------- Installation -----------------#
Copy scripts bat-terie, analysis.py and clean_bat-terie into a folder that is in your PATH variable and give it executable rights.

#----------------- Execution -----------------#
Execute bat-terie on superuser to start the test protocols with a MAC address, or hotsname, dissimilar from that of this machine.

To clean the folder execute clean_bat-terie.

#----------------- Author -----------------------#
by Malcolm AUFFRAY (Zoaldiek)
by Orso NEGRONI (Leitdorf)
by David AMIOT (Thraen)

#----------------- Copyright --------------------#
GNU General Public Licence 3

