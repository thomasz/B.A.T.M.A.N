#----------------- Installation1 -----------------#
Copy this script into a folder that is in your PATH variable and give it executable rights.

#----------------- Installation2 -----------------#
Execute the script "batco" on superuser.


#----------------- Author -----------------------#
by David Amiot
by Malcolm Auffray (Zoaldiek)
by Orso Onegroni

#----------------- Copyright --------------------#
GNU General Public Licence 3


